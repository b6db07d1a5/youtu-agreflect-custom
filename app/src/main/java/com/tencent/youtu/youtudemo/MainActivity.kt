package com.tencent.youtu.youtudemo


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tencent.youtu.youtudemo.textureView.FaceLiveActivity2
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        init()
    }

    private fun init() {
        lightLiveCheckBtn.setOnClickListener {
            val intent = Intent()
            intent.setClass(this , FaceLiveActivity2::class.java)
            this.startActivity(intent)
        }
    }

}