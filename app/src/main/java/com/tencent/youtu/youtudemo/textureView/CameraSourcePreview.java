package com.tencent.youtu.youtudemo.textureView;

/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.Manifest;
import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.RequiresPermission;
import android.util.Log;
import android.view.TextureView;

import java.io.IOException;


public class CameraSourcePreview {
    private static final String TAG = "CameraSourcePreview";

    private Context mContext;
    private TextureView textureView;
    private boolean mStartRequested;
    private CameraSource2 mCameraSource;

    private int previewWidth = 1280;
    private int previewHeight = 720;

    public CameraSourcePreview(Context context) {
        mContext = context;
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void start(CameraSource2 cameraSource) throws IOException, SecurityException {
        if (cameraSource == null) {
            stop();
        }

        mCameraSource = cameraSource;

        if (mCameraSource != null && textureView.isAvailable()) {
            mStartRequested = true;
            textureView.setOpaque(false);
            startIfReady(previewWidth, previewHeight);
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void start(CameraSource2 cameraSource, TextureView textureView, int width, int height) throws IOException, SecurityException {
        this.previewHeight = height;
        this.previewWidth = width;
        this.textureView = textureView;
        start(cameraSource);
    }

    public void stop() {
        if (mCameraSource != null) {
            mCameraSource.stop();
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    private void startIfReady(int width, int height) throws IOException, SecurityException {
        if (mStartRequested && textureView.isAvailable()) {
            mCameraSource.start(textureView, width, height);
            }
            mStartRequested = false;
    }

    private boolean isPortraitMode() {
        int orientation = mContext.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return false;
        }
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            return true;
        }

        Log.d(TAG,"isPortraitMode returning false by default");
        return false;
    }
}