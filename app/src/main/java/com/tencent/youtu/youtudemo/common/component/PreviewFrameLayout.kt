package com.tencent.youtu.youtudemo.common.component

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout

/**
 * A layout which handles the preview aspect ratio.
 */
class PreviewFrameLayout(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs) {

    private var mAspectRatio = 4.0 / 3.0

    /**
     * A callback to be invoked when the preview frame's size changes.
     */
    interface OnSizeChangedListener {
        fun onSizeChanged(ratio: Double)
    }

    fun getmAspectRatio(): Double {
        return mAspectRatio
    }

    fun setAspectRatio(ratio: Double) {
        if (ratio <= 0.0)
            throw IllegalArgumentException()
        if (mAspectRatio != ratio) {
            mAspectRatio = ratio
            requestLayout()
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    fun showBorder(enabled: Boolean) {
        isActivated = enabled
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        var previewWidth = View.MeasureSpec.getSize(widthSpec)
        var previewHeight = View.MeasureSpec.getSize(heightSpec)

        // Get the padding of the border background.
        val hPadding = paddingLeft + paddingRight
        val vPadding = paddingTop + paddingBottom

        // Resize the preview frame with correct aspect ratio.
        previewWidth -= hPadding
        previewHeight -= vPadding

        val widthLonger = previewWidth > previewHeight
        var longSide = if (widthLonger) previewWidth else previewHeight
        var shortSide = if (widthLonger) previewHeight else previewWidth
        if (longSide < shortSide * mAspectRatio) {
            longSide = (shortSide.toDouble() * mAspectRatio).toInt()
        } else {
            shortSide = (longSide.toDouble() / mAspectRatio).toInt()
        }
        if (widthLonger) {
            previewWidth = longSide
            previewHeight = shortSide
        } else {
            previewWidth = shortSide
            previewHeight = longSide
        }

        // Add the padding of the border.
        previewWidth += hPadding
        previewHeight += vPadding

        // Ask children to follow the new preview dimension.
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(previewWidth, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(previewHeight, View.MeasureSpec.EXACTLY))
    }
}
