package com.tencent.youtusdk.common.component

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.SurfaceView

import com.tencent.youtu.ytfacetrace.jni.YTFaceTraceJNIInterface


class DrawView(context: Context, attrs: AttributeSet) : SurfaceView(context, attrs) {
    private val p = Paint()
    private val p2 = Paint()

    private var faceRect: Rect? = null
    private var checkRect: Rect? = null

    init {

        p.setARGB(100, 0, 255, 0)
        p2.setARGB(100, 255, 0, 0)

        setWillNotDraw(false)
    }

    fun setRectFace(rect: Rect?) {
        faceRect = rect

        postInvalidate()
    }

    fun setRectChecker(rect: Rect?) {
        checkRect = rect

        postInvalidate()
    }

    override fun onDraw(canvas: Canvas) {
        if(faceRect != null) {
            canvas.drawRect(checkRect, p)
            canvas.drawRect(faceRect,p2)
        }
    }
}