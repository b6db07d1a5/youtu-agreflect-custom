package com.tencent.youtu.youtudemo.textureView

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.ImageFormat
import android.graphics.Matrix
import android.hardware.Camera
import android.support.annotation.RequiresPermission
import android.util.Log
import android.view.Display
import android.view.Surface
import android.view.TextureView
import android.view.WindowManager
import com.tencent.youtu.ytfacelive.YTFaceLiveInterface
import java.io.IOException
import java.lang.RuntimeException
import java.nio.ByteBuffer
import java.util.*

class CameraSource2(private val mContext: Context) {
    @SuppressLint("InlinedApi")
    private val CAMERA_FACING_BACK = Camera.CameraInfo.CAMERA_FACING_BACK
    @SuppressLint("InlinedApi")
    private val CAMERA_FACING_FRONT = Camera.CameraInfo.CAMERA_FACING_FRONT
    private var mOpenedCamera: Camera? = null
    private var mOpenedCameraId: Int = CAMERA_FACING_FRONT
    private var previewHeight: Int? = 0
    private var previewWidth: Int? = 0
    private val TAG = "OpenCameraSource"
    private var mRotation: Int = 0
    private var mPreviewSize: Camera.Size? = null
    private var mTextureView: TextureView? = null
    private var mDisplay: Display? = null
    private var focusMode: String? = null


    fun getPreviewWidth(): Int? = previewWidth
    fun getPreviewHeight(): Int? = previewHeight
    fun getCameraId(): Int = mOpenedCameraId
    fun getCamera(): Camera? = mOpenedCamera

    fun setTextureView(textureView: TextureView) {
        mTextureView = textureView
    }

    fun setDisplay(display: Display) {
        mDisplay = display
    }

    fun setFocusMode(focusMode: String?){
        this.focusMode = focusMode
    }

    @SuppressLint("InlinedApi")
    fun createCamera(width: Int, height: Int): Camera {
            val requestedCameraId = getIdForRequestedCamera(mOpenedCameraId)
            if (requestedCameraId == -1) {
                throw RuntimeException("Could not find requested camera.") as Throwable
            }

            val camera = Camera.open(requestedCameraId)

            if (camera == null) {
                throw RuntimeException("can't find any camera")
            } else {
                val parameters = camera.parameters
                mPreviewSize = calBestPreviewSize(parameters.supportedPreviewSizes,width, height)
                setRotationAndPreviewSize(camera, parameters, width, height)
                previewHeight = mPreviewSize?.height
                previewWidth = mPreviewSize?.width

                if (focusMode != null) {
                    if (parameters.supportedFocusModes.contains(
                                    focusMode)) {
                        parameters.focusMode = focusMode
                    } else {
                        Log.d(TAG, "Camera focus mode: $focusMode is not supported on this device.")
                    }
                    camera.parameters = parameters

                    camera.setPreviewCallback(CameraPreviewCallback())
//                mOpenedCamera?.addCallbackBuffer(createPreviewBuffer(mPreviewSize!!))
                }
            }
        mOpenedCamera = camera
        return camera
    }

    private inner class CameraPreviewCallback : Camera.PreviewCallback {
        override fun onPreviewFrame(data: ByteArray, camera: Camera) {
            YTFaceLiveInterface.onPreviewFrame(data, camera)
        }
    }

    @SuppressLint("MissingPermission")
    @RequiresPermission(Manifest.permission.CAMERA)
    @Throws(IOException::class)
    fun start(textureView: TextureView, width: Int, height: Int): CameraSource2 {
            mTextureView = textureView
            mOpenedCamera = createCamera(width, height)
            mOpenedCamera?.setPreviewTexture(textureView.surfaceTexture)
            mOpenedCamera?.startPreview()
        return this
    }

    fun release() {
        stop()
    }

    fun stop() {
        //        Log.i(TAG, "closeCamera start");
        if (mOpenedCamera != null) {
            try {
                mOpenedCamera!!.stopPreview()
                mOpenedCamera!!.setPreviewCallback(null)
                //                Log.i(TAG, "stop preview, not previewing");
            } catch (e: Exception) {
                e.printStackTrace()
                //                Log.i(TAG, "Error setting camera preview: " + e.toString());
            }

            try {
                mOpenedCamera!!.release()
                mOpenedCamera = null
            } catch (e: Exception) {
                e.printStackTrace()
                //                Log.i(TAG, "Error setting camera preview: " + e.toString());
            } finally {
                mOpenedCamera = null
            }
        }
    }

    private fun createPreviewBuffer(previewSize: Camera.Size): ByteArray {
        val bitsPerPixel = ImageFormat.getBitsPerPixel(ImageFormat.NV21)
        val sizeInBits = (previewSize.height * previewSize.width * bitsPerPixel).toLong()
        val bufferSize = Math.ceil(sizeInBits / 8.0).toInt() + 1
        //
        // NOTICE: This code only works when using play services v. 8.1 or higher.
        //
        // Creating the byte array this way and wrapping it, as opposed to using .allocate(),
        // should guarantee that there will be an array to work with.
        val byteArray = ByteArray(bufferSize)
        val buffer = ByteBuffer.wrap(byteArray)
        if (!buffer.hasArray() || !buffer.array()!!.contentEquals(byteArray)) {
            // I don't think that this will ever happen.  But if it does, then we wouldn't be
            // passing the preview content to the underlying detector later.
            throw IllegalStateException("Failed to create valid buffer for camera source.")
        }
        return byteArray
    }

    private fun getIdForRequestedCamera(facing: Int): Int {
        val cameraInfo = Camera.CameraInfo()
        for (i in 0 until Camera.getNumberOfCameras()) {
            Camera.getCameraInfo(i, cameraInfo)
            if (cameraInfo.facing == facing) {
                return i
            }
        }
        return -1
    }

    private fun setRotation(camera: Camera, parameters: Camera.Parameters, cameraId: Int) {
        val windowManager = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        var degrees = 0
        val rotation = windowManager.defaultDisplay.rotation
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
            else -> Log.d("Bad rotation value: %s", rotation.toString())
        }

        val cameraInfo = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, cameraInfo)

        val angle: Int
        val displayAngle: Int
        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            angle = (cameraInfo.orientation + degrees) % 360
            displayAngle = (360 - angle) % 360 // compensate for it being mirrored
        } else {  // back-facing
            angle = (cameraInfo.orientation - degrees + 360) % 360
            displayAngle = angle
        }

        // This corresponds to the rotation constants in {@link Frame}.
        mRotation = angle / 90

        camera.setDisplayOrientation(displayAngle)
        parameters.setRotation(angle)
    }

    private fun setRotationAndPreviewSize(camera: Camera, parameters: Camera.Parameters, width: Int, height: Int) {
        val ratioSurface = if (width > height) width.toFloat() / height else height.toFloat() / width
        val ratioPreview = mPreviewSize?.width!!.toFloat() / mPreviewSize?.height!!

        var scaledHeight = 0
        var scaledWidth = 0
        var scaleX = 1f
        var scaleY = 1f

        var isPortrait = false

        parameters.setPreviewSize(mPreviewSize?.width!!, mPreviewSize?.height!!)
        if (mDisplay?.rotation == Surface.ROTATION_0 || mDisplay?.rotation == Surface.ROTATION_180) {
            camera.setDisplayOrientation(if (mDisplay?.rotation == Surface.ROTATION_0) 90 else 270)
            isPortrait = true
        } else if (mDisplay?.rotation == Surface.ROTATION_90 || mDisplay?.getRotation() == Surface.ROTATION_270) {
            camera.setDisplayOrientation(if (mDisplay?.rotation == Surface.ROTATION_90) 0 else 180)
            isPortrait = false
        }
        if (isPortrait && ratioPreview > ratioSurface) {
            scaledWidth = width
            scaledHeight = (mPreviewSize?.width!!.toFloat() / mPreviewSize?.height!! * width).toInt()
            scaleX = 1f
            scaleY = scaledHeight.toFloat() / height
        } else if (isPortrait && ratioPreview < ratioSurface) {
            scaledWidth = (height / (mPreviewSize?.width!!.toFloat() / mPreviewSize?.height!!)).toInt()
            scaledHeight = height
            scaleX = scaledWidth.toFloat() / width
            scaleY = 1f
        } else if (!isPortrait && ratioPreview < ratioSurface) {
            scaledWidth = width
            scaledHeight = (width / (mPreviewSize?.width!!.toFloat() / mPreviewSize?.height!!)).toInt()
            scaleX = 1f
            scaleY = scaledHeight.toFloat() / height
        } else if (!isPortrait && ratioPreview > ratioSurface) {
            scaledWidth = (mPreviewSize?.width!!.toFloat() / mPreviewSize?.height!! * width).toInt()
            scaledHeight = height
            scaleX = scaledWidth.toFloat() / width
            scaleY = 1f
        }
        camera.parameters = parameters


        // calculate transformation matrix
        val matrix = Matrix()

        matrix.setScale(scaleX, scaleY)
        mTextureView?.setTransform(matrix)
    }


    fun getBestPreviewSize(previewSizeList: List<Camera.Size>, previewWidth: Int, previewHeight: Int): Camera.Size? {
        var bestPreviewSize: Camera.Size? = null
        for (previewSize in previewSizeList) {
            if (bestPreviewSize != null) {
                val diffBestPreviewWidth = Math.abs(bestPreviewSize.width - previewWidth)
                val diffPreviewWidth = Math.abs(previewSize.width - previewWidth)
                val diffBestPreviewHeight = Math.abs(bestPreviewSize.height - previewHeight)
                val diffPreviewHeight = Math.abs(previewSize.height - previewHeight)
                if (diffPreviewWidth + diffPreviewHeight < diffBestPreviewWidth + diffBestPreviewHeight) {
                    bestPreviewSize = previewSize
                }
            } else {
                bestPreviewSize = previewSize
            }
        }
        return bestPreviewSize
    }

    private fun calBestPreviewSize(previewSizeList: List<Camera.Size>, width: Int, height: Int): Camera.Size {
        val widthLargerSize = ArrayList<Camera.Size>()
        for (tmpSize in previewSizeList) {
            if (tmpSize.width > tmpSize.height) {
                widthLargerSize.add(tmpSize)
            }
        }

        widthLargerSize.sortWith(Comparator { lhs, rhs ->
            val off_one = Math.abs(lhs.width * lhs.height - width * height)
            val off_two = Math.abs(rhs.width * rhs.height - width * height)
            off_one - off_two
        })

        return widthLargerSize[0]
    }

}