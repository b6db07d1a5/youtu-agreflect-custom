package com.tencent.youtu.youtudemo.common.net

import com.tencent.youtu.ytagreflectlivecheck.requester.RGBConfigRequester
import com.tencent.youtu.ytfacelive.interfaces.YTRGBConfigRequest
import com.tencent.youtu.ytfacelive.interfaces.YTUploadVideoRequest
import com.tencent.youtu.ytfacelive.YTFaceLiveInterface
import java.io.IOException
import java.util.concurrent.TimeUnit

import okhttp3.Call
import okhttp3.Callback
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor


class FaceLiveUseCase private constructor() {
    private var client: OkHttpClient? = null

    init {
        initClient()
    }

    internal inner class TestInterceptor : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val builder = chain.request().newBuilder()
            addHostHead(builder)

            return chain.proceed(builder.build())
        }

        private fun addHostHead(builder: Request.Builder) {
            //增加Host
            builder.addHeader("Host", "meetingtest.youtu.qq.com")
        }
    }


    private fun initClient() {
        val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            //                Log.d("WeNet",message);
        })
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY



        client = OkHttpClient.Builder()
                .connectTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
                .addInterceptor(TestInterceptor())
                .addInterceptor(httpLoggingInterceptor)
                .build()
    }

//    interface YTFaceLiveCallback {
//        fun onSuccess(responseString: String)
//        fun onFailure(code: Int, msg: String)
//    }

    fun postData(appid: String, lightdiff: String, callback: YTUploadVideoRequest.UploadVideoRequestCallback?) {
        val requestBody = RequestBody.create(
                MediaType.parse(MEDIA_TYPE), lightdiff)

        val request = Request.Builder()
                .url(POST_URL)
                .post(requestBody)
                .build()

        client!!.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                //                Log.i(TAG,"onFailure: " + e.getMessage());
                callback?.onUploadVideoRequestFailed(-1, e.message)
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                //                Log.i(TAG,"onResponse");
                callback?.onUploadVideoRequestSuccess(response.body()?.string())
            }
        })
    }

    fun getConfig(body: String, callback: YTRGBConfigRequest.RGBConfigRequestCallBack?) {
        val requestBody = RequestBody.create(
                MediaType.parse(MEDIA_TYPE), body)

        val request = Request.Builder()
                .url(GET_CONFIG_URL)
                .post(requestBody)
                .build()

        client!!.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                //                Log.i(TAG,"onFailure: " + e.getMessage());
                callback?.onRGBConfigRequestFailed(-1, e.message)
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                //                Log.i(TAG,"onResponse");
                callback?.onRGBConfigRequestSuccess(response.body()?.string())
            }
        })
    }

    companion object {
        private val TAG = "Demo-FaceLiveUseCase"
        //    private static final String POST_URL = "http://meetingtest.youtu.qq.com/youtu/liveapi/reflect_live";    //test url
        //    private static final String GET_CONFIG_URL = "https://meetingtest.youtu.qq.com/youtu/openliveapi/get_color_seq";

        private val POST_URL = "http://203.151.132.49:38088/youtu/liveapi/reflect_live"    //test url
        private val GET_CONFIG_URL = "http://203.151.132.49:38088/youtu/liveapi/get_color_seq"

        //    private static final String POST_URL = "http://46.137.212.179:38088/youtu/liveapi/reflect_live";    //test url
        //    private static final String GET_CONFIG_URL = "http://46.137.212.179:38088/youtu/liveapi/get_color_seq";

        private val MEDIA_TYPE = "application/json"//"application/json;charset=utf-8";
        private val TIME_OUT = 60000

        private var sInstance: FaceLiveUseCase? = null

        val instance: FaceLiveUseCase
            get() {
                if (sInstance == null) {
                    sInstance = FaceLiveUseCase()
                }

                return sInstance as FaceLiveUseCase
            }
    }
}
