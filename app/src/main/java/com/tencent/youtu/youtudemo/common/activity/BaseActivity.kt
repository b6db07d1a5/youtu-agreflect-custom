package com.tencent.youtu.youtudemo.common.activity

import android.Manifest
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    private val FACE_PERMISSION_QUEST_CAMERA = 1024

    fun askForPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), FACE_PERMISSION_QUEST_CAMERA)
        } else {
            updateUI()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            FACE_PERMISSION_QUEST_CAMERA ->
                //If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        updateUI()
                        return
                    } else {
                        finish()
                        return
                    }
                }
        }
    }

    abstract fun updateUI()
}