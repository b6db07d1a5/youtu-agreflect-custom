package com.tencent.youtu.youtudemo.common.interfaces

import android.hardware.Camera

interface CustomFaceLiveInterface {
    fun initSDK()
    fun setPreviewFrame(data: ByteArray, camera: Camera)
}