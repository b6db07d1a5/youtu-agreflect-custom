package com.tencent.youtu.youtudemo.textureView

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.graphics.SurfaceTexture
import android.graphics.drawable.Drawable
import android.hardware.Camera
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.util.Log
import android.view.SurfaceHolder
import android.view.TextureView
import android.widget.Toast

import com.tencent.youtu.youtudemo.R
import com.tencent.youtu.youtudemo.R.id.wbcf_back_rl
import com.tencent.youtu.youtudemo.common.activity.BaseActivity
import com.tencent.youtu.youtudemo.common.net.FaceLiveUseCase
import com.tencent.youtu.ytagreflectlivecheck.YTAGReflectLiveCheckInterface
import com.tencent.youtu.ytagreflectlivecheck.YTAGReflectLiveCheckInterface.LightLiveProcessState.HANGUP
import com.tencent.youtu.ytagreflectlivecheck.requester.LightDiffResponse
import com.tencent.youtu.ytcommon.tools.YTThreadOperate
import com.tencent.youtu.ytfacetrace.YTFaceTraceInterface
import kotlinx.android.synthetic.main.wbyt_face_verify_layout2.*
import java.io.IOException
import java.util.*
import com.tencent.youtu.ytagreflectlivecheck.YTAGReflectLiveCheckInterface.LightLiveProcessState.REFLECTING
import com.tencent.youtu.ytcommon.YTCommonExInterface
import com.tencent.youtu.ytcommon.tools.YTCameraSetting
import com.tencent.youtu.ytfacelive.interfaces.YTFaceTraceResult
import com.tencent.youtu.ytfacelive.interfaces.YTRGBConfigRequest
import com.tencent.youtu.ytfacelive.interfaces.YTReflectLiveResult
import com.tencent.youtu.ytfacelive.interfaces.YTUploadVideoRequest
import com.tencent.youtu.ytfacelive.YTFaceLiveInterface
import com.tencent.youtu.ytfacetrace.YTFaceAdviseInterface
import com.tencent.youtu.ytfacetrace.YTFaceAdviseInterface.ShelterInFrame.*
import com.tencent.youtu.ytfacetrace.YTFaceTraceInterface.ShelterJudge.SHELTER_PASS
import com.tencent.youtu.ytfacetrace.jni.YTFaceTraceJNIInterface
import com.tencent.youtu.ytposedetect.YTPoseDetectInterface
import com.tencent.youtu.ytposedetect.YTPoseDetectInterface.PoseDetectLiveType.LIVETYPE_BLINK_EYE
import permissions.dispatcher.*
import java.lang.Exception

@RuntimePermissions
class FaceLiveActivity2 : BaseActivity(),
        YTFaceTraceResult, YTFaceTraceInterface.FaceTraceingNotice,
        YTRGBConfigRequest, YTUploadVideoRequest,
        YTReflectLiveResult,
        TextureView.SurfaceTextureListener {

    private val TAG = "RevlectLiveCheckDemo"
    private var originalFactRectDrawable: Drawable? = null
    private var tintFactRectDrawable: Drawable? = null
    private var mTimer: Timer? = null
    private var checkedRect: Rect? = null

    private var mCameraSource: CameraSource2? = null
    private var mCameraPreview: CameraSourcePreview? = null

//    private var mask_height: Int = 0
//    private var mask_width: Int = 0
//    private var mask_height_ratio: Float = 0.toFloat()
//    private var mask_width_ratio: Float = 0.toFloat()

    companion object {
        const val FAR_PROPORTION = 0.1f
        const val CLOSE_PROPORTION = 0.75f
        const val DEFAULT_COLOR1 = "#e94b2c"
        const val DEFAULT_COLOR2 = "#e94b2c"
    }

    private var isChecking = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wbyt_face_verify_layout2)

        supportActionBar?.hide()

        wbcf_back_rl.setOnClickListener {
            clickBack()
        }
        if (Build.VERSION.SDK_INT >= 23) {
            Log.d(TAG, "begin askForPermission the sdk version is" + Build.VERSION.SDK_INT)
            askForPermission()
        } else {
            Log.d(TAG, "no need to askForPermission the sdk version is" + Build.VERSION.SDK_INT)
            updateUI()
        }
    }

    override fun updateUI() {
        val r = YTFaceLiveInterface.initYoutuInstance(this, "dev_youtudemo_20190419.lic")
        if (r != 0) {
            popTip("Auth check failed", "")
        } else {
            init()
        }
    }

    override fun onSurfaceTextureSizeChanged(holder: SurfaceTexture?, p1: Int, p2: Int) {

    }

    override fun onSurfaceTextureUpdated(holder: SurfaceTexture?) {

    }

    override fun onSurfaceTextureDestroyed(holder: SurfaceTexture?): Boolean {
        return false
    }

    override fun onSurfaceTextureAvailable(holder: SurfaceTexture?, width: Int, height: Int) {
        addPreviewSource(width, height)
    }


    private fun init() {
        originalFactRectDrawable = ContextCompat.getDrawable(this, R.mipmap.wbcf_face_detect_outline)
        tintFactRectDrawable = DrawableCompat.wrap(originalFactRectDrawable!!).mutate()

        DrawableCompat.setTint(tintFactRectDrawable!!, Color.parseColor("#ffffff"))
        readOutlineBg!!.setImageDrawable(tintFactRectDrawable)

    }

    override fun onStartFaceTraceSuccess() {

    }

    override fun onStartFaceTraceFailed(errorCode: Int, message: String?, tips: String?) {

    }

    private fun startDetect() {
        checkedRect = Rect(preview.left, preview.top, preview.right, preview.bottom)
        drawView.setRectChecker(checkedRect)

        YTFaceLiveInterface.startFaceTrace(this, mCameraSource?.getCamera(), mCameraSource?.getCameraId()!!, this, this)
    }

    override fun onTracing(facePreviewingNoticeCode: Int, facePreviewStatus: YTFaceTraceJNIInterface.FaceStatus?, faceInPreviewFrame: Rect?, data: ByteArray?, camera: Camera?) {
        if (facePreviewingNoticeCode == YTFaceTraceInterface.FaceTraceingNotice.PREVIEWING_START) {

            //YTFaceLiveInterface.startPoseDetect(this, mCameraSource?.getCamera(), mCameraSource?.getCameraId()!!, this)

            stopTimer()
            onTracingResult("ไม่พบใบหน้า", DEFAULT_COLOR1, DEFAULT_COLOR2)
        } else if (facePreviewingNoticeCode == YTFaceTraceInterface.FaceTraceingNotice.PREVIEWING_STOP) {
            stopTimer()
        } else if (facePreviewingNoticeCode == YTFaceTraceInterface.FaceTraceingNotice.PREVIEWING_ONPREVIEW) {
            //val advise = YTFaceLiveInterface.getPreviewAdvise(facePreviewStatus, faceInPreviewFrame, checkedRect, FAR_PROPORTION, CLOSE_PROPORTION)

            val advise = YTFaceAdviseInterface.shelterInFrame(facePreviewStatus, faceInPreviewFrame, checkedRect, FAR_PROPORTION, CLOSE_PROPORTION);
            //YTFaceLiveInterface.poseDetect(facePreviewStatus, LIVETYPE_BLINK_EYE, data, camera, true, this)

            when (advise) {
                SHELTER_TOO_FAR -> {
                    stopTimer()
                    onTracingResult("กรุณาขยับหน้าเข้ามาใกล้ๆ", DEFAULT_COLOR1, DEFAULT_COLOR2)
                }

                SHELTER_TOO_CLOSE -> {
                    stopTimer()
                    onTracingResult("กรุณาขยับหน้าออกไป", DEFAULT_COLOR1, DEFAULT_COLOR2)
                }

                SHELTER_NOT_IN_RECT -> {
                    stopTimer()
                    onTracingResult("กรุณาวางหน้าให้พอดีกับกรอบ", DEFAULT_COLOR1, DEFAULT_COLOR2)

                }

                SHELTER_INCORRECT_POSTURE -> {
                    stopTimer()
                    onTracingResult("กรุณาวางหน้าให้ตรง", DEFAULT_COLOR1, DEFAULT_COLOR2)
                }
                SHELTER_NO_FACE -> {
                    stopTimer()
                    onTracingResult("ไม่พบใบหน้า", DEFAULT_COLOR1, DEFAULT_COLOR2)
                }

                SHELTER_PASS -> {
                    if (!isChecking) {
                        startAGReflectTimer()
                    }
                    onTracingResult("กรุณาค้างไว้", "#409eff", "#409eff")
                }
            }
        }
    }

    private fun onTracingResult(message: String, color1: String, color2: String) {
        mFaceCommandTv!!.text = message
        mFaceCommandTv!!.setTextColor(Color.parseColor(color1))
        DrawableCompat.setTint(tintFactRectDrawable!!, Color.parseColor(color2))
        readOutlineBg!!.setImageDrawable(tintFactRectDrawable)
    }

    override fun requestRGBConfig(bodyRequset: String?, callback: YTRGBConfigRequest.RGBConfigRequestCallBack?) {
        FaceLiveUseCase.instance.getConfig(bodyRequset.toString(), callback)
    }

    override fun requestUploadVideo(bodyRequset: String?, callback: YTUploadVideoRequest.UploadVideoRequestCallback?) {
        runOnUiThread { mFaceCommandTv!!.text = "กำลังตรวจสอบ" }
        FaceLiveUseCase.instance.postData("", bodyRequset.toString(), callback)
    }

    override fun onReflectLiveCheckSuccess(isHuman: Boolean, msg: String?) {
        val msg: String
        if (isHuman) {
            msg = "Check Passed"
        } else {
            msg = "Check Failed"
        }
        popTip(msg, "")
    }

    override fun onReflectLiveCheckFailed(errorCode: Int, msg: String?) {
        runOnUiThread {
                        mFaceCommandTv!!.text = "ผิดพลาด:"
        }
        if (errorCode == 303) {
            popTip("ไม่พบใบหน้าในการตรวจสอบ", "")
        } else {
            popTip("ตรวจสอบผิดพลาด.", msg.toString())
        }
    }

    private fun clickBack() {
        if (YTFaceLiveInterface.isFaceTracing()) {
            YTFaceLiveInterface.stopFaceTrace()
        }
        if (YTFaceLiveInterface.getLiveCheckState() == REFLECTING) {
            YTFaceLiveInterface.stopReflectLiveCheck()
        }
        finish()
    }

    private fun startAGReflectTimer() {
        isChecking = true

        mTimer = Timer()
        mTimer!!.schedule(object : TimerTask() {
            override fun run() {
                YTThreadOperate.runOnUiThread {
                    if (mTimer != null) {
                        mTimer!!.cancel()
                        mTimer = null
                    }

                    startAGReflect()
                }
            }
        }, 2000, 2000)
    }

    private fun startAGReflect() {
        DrawableCompat.setTint(tintFactRectDrawable!!, Color.parseColor("#409eff"))
        readOutlineBg!!.setImageDrawable(tintFactRectDrawable)
        if (mCameraSource?.getCamera()?.parameters != null) {
            YTFaceLiveInterface.setRGBConfigRequest(this)
            YTFaceLiveInterface.setUploadVideoRequest(this)
            YTFaceLiveInterface.startReflectLiveCheck(this, mCameraSource?.getCamera(), mCameraSource?.getCameraId()!!, mYTReflectLayout, this)
        } else {
            popTip("android.hardware.Camera.getParameters()' on a null object reference", "")
        }
    }


    private fun startPreview() {
        if (isCameraSupport()) {
            createCameraSource(true)
            startCameraSource()
        }
    }

    @Throws(SecurityException::class)
    private fun startCameraSource() {
        if (mCameraSource != null && preview.isAvailable) {
            addPreviewSource(preview.width, preview.height)
        }
    }

    @SuppressLint("MissingPermission")
    private fun addPreviewSource(width: Int, height: Int) {
        try {
            mCameraPreview?.start(
                    mCameraSource,
                    preview,
                    width,
                    height
            )
            startDetect()
        } catch (e: IOException) {
            mCameraSource?.release()
            mCameraSource = null
        } catch (e: RuntimeException) {
            mCameraSource?.release()
            mCameraSource = null
        }
    }

    private fun stopTimer() {
        isChecking = false

        if (mTimer != null) {
            mTimer!!.cancel()
            mTimer = null
        }
    }

    private fun popTip(str: String, errorMsg: String = "") {
        YTThreadOperate.runOnUiThread {
            val builder = AlertDialog.Builder(this)
            builder.setMessage("$str \n$errorMsg")
            builder.setCancelable(false)
            builder.setPositiveButton("ok") { dialog, id ->
                finish()
            }
            if (!isFinishing) {
                builder.show()
            }
        }
    }

    override fun onResume() {
        Log.d(TAG, "Activity onResume")
        super.onResume()
        mCameraPreview = CameraSourcePreview(this)
        startPreview()
    }

    override fun onPause() {
        Log.d(TAG, "Activity onPause")
        super.onPause()
    }

    override fun onStop() {
        Log.d(TAG, "Activity onStop")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(TAG, "Activity onDestroy")
        super.onDestroy()
        mCameraSource?.stop()
        mCameraSource = null
    }

    @SuppressLint("InlinedApi")
    @NeedsPermission(Manifest.permission.CAMERA)
    fun createCameraSource(useFocusMode: Boolean) {
        preview.surfaceTextureListener = this
        mCameraSource = CameraSource2(this)
        mCameraSource?.setTextureView(preview)
        mCameraSource?.setDisplay(windowManager.defaultDisplay)
        mCameraSource?.setFocusMode(if (useFocusMode) Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE else null)
    }

    @SuppressLint("NoCorrespondingNeedsPermission")
    @OnShowRationale(Manifest.permission.CAMERA)
    fun showRationaleForStorage(request: PermissionRequest) {
//        android.support.v7.app.AlertDialog.Builder(this)
//                .setTitle(R.string.PERMISSION_TITLE)
//                .setMessage(R.string.permission_camera_denied)
//                .setPositiveButton(R.string.button_allow) { _, _ -> request.proceed() }
//                .setNegativeButton(R.string.button_deny) { _, _ -> request.cancel() }
//                .show()
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    fun showDeniedForStorage() {
//        Toast.makeText(this, R.string.permission_write_ex_denied, Toast.LENGTH_SHORT).show()
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    fun showNeverAskForStorage() {
//        Toast.makeText(this, R.string.permission_write_ex_never_ask, Toast.LENGTH_SHORT).show()
    }


    private fun isCameraSupport(): Boolean {
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
    }
}